"use strict";

var express = require('express');

var app = express();
var port = 3000;

var mysql = require('mysql');

var bodyParser = require('body-parser');

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'blog'
});
connection.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");
}); //app.get('/',(req,res) => res.send('hello world'));
//for person

app.get('/login', function (req, res) {
  res.render("login.pug", {
    title: "Express"
  });
});
app.post("/login", function (req, res, next) {
  var data = {
    email: req.body.email,
    pass: req.body.password
  };
  console.log(data);
  var sql = "SELECT * FROM MST_Person_Information WHERE email = ? AND password = ?";
  connection.query(sql, [data.email, data.pass], function (err, result) {
    if (result.length > 0) {
      res.render('result.pug', {
        people: result
      });
      console.log(result);
    } else {
      res.render("notfound.pug", {
        title: "Express"
      });
    }
  });
});
app.get('/person', function (req, res) {
  connection.query('SELECT * FROM MST_Person_Information', function (err, result) {
    res.render('person.pug', {
      people: result
    });
  });
});
app.get('/person/add', function (req, res) {
  res.render('addPerson.pug');
});
app.post('/person/add', function (req, res) {
  var name = req.body.name;
  var age = req.body.age;
  var movie = req.body.movie;
  var email = req.body.email;
  var password = req.body.password;
  var person = {
    name: name,
    age: age,
    movie: movie,
    email: email,
    password: password
  };
  connection.query('INSERT INTO MST_Person_Information SET ?', person, function (err) {
    console.log('Data Inserted');
    return res.redirect('/person');
  });
});
app.get('/person/edit/:id', function (req, res) {
  var edit_postID = req.params.id;
  connection.query('SELECT * FROM MST_Person_Information WHERE id=?', [edit_postID], function (err, results) {
    if (results) {
      res.render('editPerson.pug', {
        person: results[0]
      });
    }
  });
});
app.post('/person/edit/:id', function (req, res) {
  var name = req.body.name;
  var age = req.body.age;
  var movie = req.body.movie;
  var email = req.body.email;
  var password = req.body.password;
  var userId = req.params.id;
  connection.query('UPDATE `MST_Person_Information` SET name = ?, age = ?, movie = ?,email = ?, password = ? WHERE id = ?', [name, age, movie, email, password, userId], function (err, results) {
    if (results.changedRows === 1) {
      console.log('Post Updated');
    }

    return res.redirect('/person');
  });
});
app.get('/person/delete/:id', function (req, res) {
  connection.query('DELETE FROM `MST_Person_Information` WHERE id = ?', [req.params.id], function (err, results) {
    return res.redirect('/person');
  });
}); //for articles

app.get('/', function (req, res) {
  connection.query('SELECT * FROM articles', function (err, result) {
    res.render('index', {
      articles: result
    });
  });
});
app.get('/add', function (req, res) {
  res.render('add');
});
app.post('/add', function (req, res) {
  var title = req.body.title;
  var content = req.body.body;
  var article = {
    title: title,
    body: content,
    author_id: 1,
    update_ts: new Date()
  };
  connection.query('INSERT INTO articles SET ?', article, function (err) {
    console.log('Data Inserted');
    return res.redirect('/');
  });
});
app.get('/edit/:id', function (req, res) {
  var edit_postID = req.params.id;
  connection.query('SELECT * FROM articles WHERE atc_id=?', [edit_postID], function (err, results) {
    if (results) {
      res.render('edit', {
        articles: results[0]
      });
    }
  });
});
app.post('/edit/:id', function (req, res) {
  var update_title = req.body.title;
  var update_body = req.body.body;
  var update_ts = new Date();
  var userId = req.params.id;
  connection.query('UPDATE `articles` SET title = ?, body = ?, update_ts = ? WHERE atc_id = ?', [update_title, update_body, update_ts, userId], function (err, results) {
    if (results.changedRows === 1) {
      console.log('Post Updated');
    }

    return res.redirect('/');
  });
});
app.get('/delete/:id', function (req, res) {
  connection.query('DELETE FROM `articles` WHERE atc_id = ?', [req.params.id], function (err, results) {
    return res.redirect('/');
  });
});
app.listen(port, function () {
  return console.log("Example app listening on port ".concat(port, "!"));
});